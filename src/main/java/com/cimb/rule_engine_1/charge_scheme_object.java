package com.cimb.rule_engine_1;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class charge_scheme_object implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("Customer_Type")
	private java.lang.String customer_type;
	@org.kie.api.definition.type.Label("Transaction_Count")
	private java.lang.Integer transaction_count;
	@org.kie.api.definition.type.Label("Transaction_Type")
	private java.lang.String transaction_type;
	@org.kie.api.definition.type.Label("Fee")
	private java.lang.Integer fee;

	@org.kie.api.definition.type.Label("Transaction_Amount")
	private java.lang.Integer transaction_amount;

	@org.kie.api.definition.type.Label("Conditioned")
	private java.lang.Boolean conditioned;

	@org.kie.api.definition.type.Label(value = "Business_schema")
	private java.lang.String business_schema;

	public charge_scheme_object() {
	}

	public java.lang.String getCustomer_type() {
		return this.customer_type;
	}

	public void setCustomer_type(java.lang.String customer_type) {
		this.customer_type = customer_type;
	}

	public java.lang.Integer getTransaction_count() {
		return this.transaction_count;
	}

	public void setTransaction_count(java.lang.Integer transaction_count) {
		this.transaction_count = transaction_count;
	}

	public java.lang.String getTransaction_type() {
		return this.transaction_type;
	}

	public void setTransaction_type(java.lang.String transaction_type) {
		this.transaction_type = transaction_type;
	}

	public java.lang.Integer getFee() {
		return this.fee;
	}

	public void setFee(java.lang.Integer fee) {
		this.fee = fee;
	}

	public java.lang.Integer getTransaction_amount() {
		return this.transaction_amount;
	}

	public void setTransaction_amount(java.lang.Integer transaction_amount) {
		this.transaction_amount = transaction_amount;
	}

	public java.lang.Boolean getConditioned() {
		return this.conditioned;
	}

	public void setConditioned(java.lang.Boolean conditioned) {
		this.conditioned = conditioned;
	}

	public java.lang.String getBusiness_schema() {
		return this.business_schema;
	}

	public void setBusiness_schema(java.lang.String business_schema) {
		this.business_schema = business_schema;
	}

	public charge_scheme_object(java.lang.String customer_type,
			java.lang.Integer transaction_count,
			java.lang.String transaction_type, java.lang.Integer fee,
			java.lang.Integer transaction_amount,
			java.lang.Boolean conditioned, java.lang.String business_schema) {
		this.customer_type = customer_type;
		this.transaction_count = transaction_count;
		this.transaction_type = transaction_type;
		this.fee = fee;
		this.transaction_amount = transaction_amount;
		this.conditioned = conditioned;
		this.business_schema = business_schema;
	}

}